Module Name
===========

Teaser text

Usage
=====

To use this module, you need to:

Some words about usage

Known issues
============

* Issue 1
* Issue 2

Credits
=======

Authors
-------

* Jamotion GmbH

Contributors
------------

* Renzo Meister <rm@jamotion.ch>
* Sébastien Pulver <rm@jamotion.ch>
