{
    'name': "Flectra_Community Base Additions",

    'summary': """Base additions for managing flectra community""",
    'author': "Jamotion GmbH",
    'website': "https://jamotion.ch",
    'category': 'Specific Industry Applications',
    'version': '1.0.1.0.0',
    'depends': [
        'base',
    ],
    'data': [
        'data/data.xml',
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}