# -*- coding: utf-8 -*-
from flectra import http

# class FlectraCommunity(http.Controller):
#     @http.route('/flectra_community/flectra_community/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/flectra_community/flectra_community/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('flectra_community.listing', {
#             'root': '/flectra_community/flectra_community',
#             'objects': http.request.env['flectra_community.flectra_community'].search([]),
#         })

#     @http.route('/flectra_community/flectra_community/objects/<model("flectra_community.flectra_community"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('flectra_community.object', {
#             'object': obj
#         })